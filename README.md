# Wordlists for Python

Miscellaneous wordlists so that I don't have to parse the same text files over and over:

* BIP39 words
* EFF's “new” wordlists
* PGP wordlist
* S/KEY words

One day these will have a `Makefile` and will be installable with e.g. `pip`.
