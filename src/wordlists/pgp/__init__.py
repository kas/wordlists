# Copyright © 2023 Klaus Alexander Seistrup <klaus@seistrup.dk>

from .words import PGP_WORDS

__all__ = (
    'PGP_WORDS',
    'PGP_WORDS_ODD',
    'PGP_WORDS_EVEN',
)

PGP_WORDS_EVEN = tuple(word for (word, _) in PGP_WORDS)
PGP_WORDS_ODD  = tuple(word for (_, word) in PGP_WORDS)

# eof
