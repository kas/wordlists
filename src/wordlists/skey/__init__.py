# Copyright © 2023 Klaus Alexander Seistrup <klaus@seistrup.dk>

from .words import SKEY_WORDS

__all__ = (
    'SKEY_WORDS',
    'SKEY_WORDS_LOWER',
    'SKEY_WORDS_UPPER',
)

SKEY_WORDS_LOWER = (word.casefold() for word in SKEY_WORDS)
SKEY_WORDS_UPPER = SKEY_WORDS

# eof
