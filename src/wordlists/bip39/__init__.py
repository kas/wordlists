# Copyright © 2023 Klaus Alexander Seistrup <klaus@seistrup.dk>

from .words import BIP39_WORDS

__all__ = (
    'BIP39_WORDS',
)

# eof
